FROM node:lts-alpine As development

RUN apk upgrade
RUN apk add --update bash
RUN apk add --update alpine-sdk

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . .

RUN npm install --only=development
# RUN npm run build
CMD npm run start:dev

FROM node:lts-alpine AS production


ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm ci --only=production

COPY --from=development /usr/src/app/dist ./dist

CMD ["node", "dist/main"]
