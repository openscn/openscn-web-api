import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Logger,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { PublishDebuggingWorkerEventDTO } from '../dto/iot-task.dto';
import { InjectQueue } from '@nestjs/bullmq';
import { Queue } from 'bullmq';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IotWorkerJobsQueueEnum, IsDevModeGuard } from '../../common';
import { ConfigService } from '@nestjs/config';
import { IConfiguration } from '../../config/config.inteface';
import { IoTTaskService } from '../services/iot-task/iot-task.service';

@Controller('iot-task/debug/')
@ApiTags('iot-task')
@ApiBearerAuth()
@UseGuards(IsDevModeGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class IoTTaskController {
  private readonly _logger = new Logger(IoTTaskController.name);

  constructor(
    private readonly _configService: ConfigService,
    @InjectQueue(IotWorkerJobsQueueEnum.Current)
    private readonly _queue: Queue,
    private readonly _iotTaskService: IoTTaskService,
  ) {
    const env: IConfiguration['app']['environment'] =
      this._configService.get('app.environment');

    if (env === 'dev') {
      setImmediate(() => {
        this._logger.warn(
          `Running in dev mode. ${IoTTaskController.name} is available with debugging endpoints`,
        );
      });
    }
  }

  @Get('list-remote-tasks')
  async listRemoteTasks() {
    const recurringJobs = await this._queue.getRepeatableJobs();

    return {
      data: recurringJobs,
    };
  }

  @Post('publish-alert-check-threshold')
  async createEvent(@Body() dto: PublishDebuggingWorkerEventDTO) {
    const job = await this._queue.add('AlertCheckThreshold', {
      name: 'AlertCheckThreshold',
      payload: {
        ...dto,
        start: dto.start ?? '-5m',
        stop: dto.stop ?? 'now()',
      },
    });

    return {
      data: {
        job: {
          id: job.id,
          name: job.name,
          key: job.repeatJobKey,
          payload: job.data,
        },
      },
    };
  }

  @Post('reset-remote-jobs')
  async recreateRemoteJobs() {
    await this._iotTaskService.removeAllRemoteTasks();
    await this._iotTaskService.synchronizeTasks();

    return {
      data: 'ok',
    };
  }
}
