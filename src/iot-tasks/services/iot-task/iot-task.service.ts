import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  DeepPartial,
  FindOneOptions,
  FindOptionsWhere,
  Repository,
} from 'typeorm';
import { EntityService, IotWorkerJobsQueueEnum } from '../../../common';
import {
  IoTTaskCheckEntity,
  IoTTaskCheckThresholdEntity,
  IoTTaskEntity,
} from '../../entities';
import { IoTTaskCheckService } from '../iot-task-check/iot-task-check.service';
import { IoTTaskCheckThresholdService } from '../iot-check-threshold/iot-task-check-threshold.service';
import { InjectQueue } from '@nestjs/bullmq';
import { Queue } from 'bullmq';

@Injectable()
export class IoTTaskService extends EntityService<IoTTaskEntity> {
  private readonly _logger = new Logger(IoTTaskService.name);

  constructor(
    @InjectRepository(IoTTaskEntity)
    private readonly _iotTaskRepository: Repository<IoTTaskEntity>,
    @InjectQueue(IotWorkerJobsQueueEnum.Current)
    private readonly _jobQueue: Queue,
    private readonly _ioTTaskCheckService: IoTTaskCheckService,
    private readonly _ioTTaskCheckThresholdService: IoTTaskCheckThresholdService,
  ) {
    super(IoTTaskService.name);
    this.repository = _iotTaskRepository;

    this.synchronizeTasks().catch((err) =>
      this._logger.error(err.stack ?? err),
    );
  }

  async save(item: DeepPartial<IoTTaskEntity>) {
    const result = await super.save(item);

    this.handleRemoteTask(result).catch((err) =>
      this._logger.error(err.stack ?? err),
    );

    return result;
  }

  async create(item: DeepPartial<IoTTaskEntity>): Promise<IoTTaskEntity> {
    const task = new IoTTaskEntity(item);
    const validation = task.validate();

    if (!validation.isValid) {
      throw new Error(validation.validationErrors.join(', '));
    }

    return task;
  }

  async softRemove(id: number | FindOptionsWhere<IoTTaskEntity>) {
    let task;

    if (typeof id === 'number') {
      task = await this.findOne({
        where: {
          id: id,
        },
        relations: ['owner'],
      });
    } else {
      task = await this.findOne({
        where: id,
        relations: ['owner'],
      });
    }

    await this.removeRemoteTask(task);
    await this.deleteDeps(task);

    return await super.softRemove(id);
  }

  async createAndSave(
    item: DeepPartial<IoTTaskEntity>,
  ): Promise<IoTTaskEntity> {
    const instance = await this.create(item);
    return this.save(instance);
  }

  async resolveConfigurationData(task: IoTTaskEntity): Promise<IoTTaskEntity> {
    const queries = [
      this._ioTTaskCheckService.findOne({
        where: {
          identifier: task.configuration,
        },
      }),
      this._ioTTaskCheckThresholdService.findOne({
        where: {
          identifier: task.configurationDetails,
        },
      }),
    ];

    const results = await Promise.all(queries);

    task.configurationData = results[0];
    task.configurationDetailsData = results[1];

    return task;
  }

  async handleRemoteTask(task: IoTTaskEntity) {
    if (task.enabled) {
      await this.removeRemoteTask(task);
      return this.createRemoteTask(task);
    } else if (task.enabled === false) {
      return this.removeRemoteTask(task);
    }
  }

  async createRemoteTask(task: IoTTaskEntity) {
    const taskWithConfig = await this.resolveConfigurationData(task);
    const condition = (taskWithConfig.configurationDetailsData as any)
      .condition;
    const operator = condition === 'greater' ? 'ge' : 'le';

    await this._jobQueue.add(
      task.identifier,
      {
        name: task.additionalType,
        payload: {
          measurement: task.measurement,
          start: '-1h',
          stop: 'now()',
          criticalThreshold: (taskWithConfig.configurationDetailsData as any)
            .criticalThreshold,
          warningThreshold: (taskWithConfig.configurationDetailsData as any)
            .warningThreshold,
          operator: operator,
          task: task.identifier,
        },
      },
      {
        repeat: {
          pattern: task.interval,
        },
      },
    );

    this._logger.log(`Created recurring job for ${task.identifier}`);
  }

  async removeRemoteTask(task: IoTTaskEntity) {
    const repeatable = await this._jobQueue.getRepeatableJobs();

    const jobsToRemove = [];
    const reqs = [];

    for (let i = 0; i < repeatable.length; i++) {
      if (repeatable[i].name === task.identifier) {
        jobsToRemove.push(repeatable[i]);
      }
    }

    for (let i = 0; i < jobsToRemove.length; i++) {
      await this._jobQueue.removeRepeatableByKey(jobsToRemove[i].key);
      reqs.push(
        this._jobQueue
          .removeRepeatableByKey(jobsToRemove[i].key)
          .then(() =>
            this._logger.log(`Removed recurring job for ${task.identifier}`),
          )
          .catch((err) => this._logger.error(err.stack ?? err)),
      );
    }

    await Promise.all(reqs);
  }

  async synchronizeTasks(task?: FindOneOptions<IoTTaskEntity>) {
    const [remoteTasks, tasks] = await Promise.all([
      this._jobQueue.getRepeatableJobs(),
      this._iotTaskRepository.find(task),
    ]);

    const remoteTasksMap = {};
    for (let i = 0; i < remoteTasks.length; i++) {
      remoteTasksMap[remoteTasks[i].name] = remoteTasks[i];
    }

    const toCreateTasks = [];
    const removeJobs = [];

    for (let i = 0; i < tasks.length; i++) {
      if (tasks[i].enabled && !remoteTasksMap[tasks[i].identifier]) {
        toCreateTasks.push(tasks[i]);
      } else if (!tasks[i].enabled && remoteTasksMap[tasks[i].identifier]) {
        removeJobs.push(
          this._jobQueue
            .removeRepeatableByKey(remoteTasksMap[tasks[i].identifier].key)
            .then((result) => {
              this._logger.log(
                `Removed repeatable job ${tasks[i].identifier}: ${result}`,
              );
            }),
        );
      }
    }

    await Promise.all(removeJobs).catch((err) =>
      this._logger.error(err.stack ?? err),
    );

    const tasksWithConfigQueries = [];
    for (let i = 0; i < toCreateTasks.length; i++) {
      tasksWithConfigQueries.push(
        this.resolveConfigurationData(toCreateTasks[i]),
      );
    }

    const taskWithConfig = await Promise.all(tasksWithConfigQueries);

    const createJobs = [];
    for (let i = 0; i < taskWithConfig.length; i++) {
      const iotTask = taskWithConfig[i];
      const condition = (iotTask.configurationDetailsData as any).condition;
      const operator = condition === 'greater' ? 'ge' : 'le';
      createJobs.push(
        this._jobQueue
          .add(
            `${iotTask.identifier}`,
            {
              name: iotTask.additionalType,
              payload: {
                measurement: iotTask.measurement,
                start: '-1h',
                stop: 'now()',
                criticalThreshold: (iotTask.configurationDetailsData as any)
                  .criticalThreshold,
                warningThreshold: (iotTask.configurationDetailsData as any)
                  .warningThreshold,
                operator: operator,
                task: iotTask.identifier,
              },
            },
            {
              repeat: {
                pattern: iotTask.interval,
              },
            },
          )
          .then((job) => {
            this._logger.log(`Created repeatable job ${job.name}`);
          }),
      );
    }

    Promise.all(createJobs).catch((err) =>
      this._logger.error(err.stack ?? err),
    );
  }

  async saveDeps(
    configuration: DeepPartial<IoTTaskCheckEntity>,
    details: DeepPartial<IoTTaskCheckThresholdEntity>,
  ): Promise<(IoTTaskCheckEntity | IoTTaskCheckThresholdEntity)[]> {
    return await Promise.all([
      this._ioTTaskCheckService.createAndSave(configuration),
      this._ioTTaskCheckThresholdService.createAndSave(details),
    ]);
  }

  async updateDetails(
    task: IoTTaskEntity,
    update: DeepPartial<IoTTaskCheckThresholdEntity>,
  ) {
    const details = await this._ioTTaskCheckThresholdService.findOne({
      where: {
        identifier: task.configurationDetails,
      },
      relations: ['owner'],
    });
    Object.keys(update).forEach(
      (key) => update[key] === undefined && delete update[key],
    );

    if (Object.keys(update).length > 0) {
      const updatedDetails = await this._ioTTaskCheckThresholdService.create({
        ...details,
        ...update,
      });

      await this._ioTTaskCheckThresholdService.save(updatedDetails);

      await this.removeRemoteTask(task);
      await this.createRemoteTask(task);
    }
  }

  async deleteDeps(task: IoTTaskEntity) {
    const reqs = [
      this._ioTTaskCheckService.findOne({
        where: {
          owner: {
            id: task.owner.id,
          },
          identifier: task.configuration,
        },
      }),
      this._ioTTaskCheckThresholdService.findOne({
        where: {
          owner: {
            id: task.owner.id,
          },
          identifier: task.configurationDetails,
        },
      }),
    ];
    const [config, details] = await Promise.all(reqs);

    const deleteReqs = [
      this._ioTTaskCheckService.remove(config?.id),
      this._ioTTaskCheckThresholdService.remove(details?.id),
    ];

    await Promise.all(deleteReqs);
  }

  async removeAllRemoteTasks() {
    const [remoteTasks, tasks] = await Promise.all([
      this._jobQueue.getRepeatableJobs(),
      this._iotTaskRepository.find(),
    ]);

    const remoteTasksMap = {};
    for (let i = 0; i < remoteTasks.length; i++) {
      remoteTasksMap[remoteTasks[i].name] = remoteTasks[i];
    }

    const removeJobs = [];

    for (let i = 0; i < tasks.length; i++) {
      if (remoteTasksMap[tasks[i].identifier]) {
        removeJobs.push(
          this._jobQueue
            .removeRepeatableByKey(remoteTasksMap[tasks[i].identifier].key)
            .then((result) => {
              this._logger.log(
                `Removed repeatable job ${tasks[i].identifier}: ${result}`,
              );
            }),
        );
      }
    }

    await Promise.allSettled(removeJobs);
  }
}
