import { EntityValidationResult } from '../../common';
import { Column, DeepPartial, Entity } from 'typeorm';
import {
  IoTTaskConfigurationDetailsType,
  IoTTaskConfigurationType,
  IoTTaskType,
} from '../types';
import { OwnedThing } from '../../users/entities/owned-thing.entity';

@Entity({
  name: 'IoTTask_IoTTask',
})
export class IoTTaskEntity extends OwnedThing {
  constructor(props?: DeepPartial<IoTTaskEntity>) {
    super(props);

    if (props) {
      this.additionalType = props.additionalType;
      this.interval = props.interval;
      this.enabled = props.enabled;
      this.measurement = props.measurement;
      this.configurationType = props.configurationType;
      this.detailsType = props.detailsType;
      this.configuration = props.configuration;
      this.configurationDetails = props.configurationDetails;
    }
  }

  @Column({
    nullable: true,
  })
  additionalType?: IoTTaskType;

  @Column({
    nullable: false,
    default: '*/1 * * * *',
  })
  interval?: string;

  @Column({
    nullable: false,
    default: true,
  })
  enabled?: boolean;

  @Column({
    nullable: false,
  })
  measurement?: string;

  @Column({
    nullable: false,
    enum: IoTTaskConfigurationType,
  })
  configurationType?: IoTTaskConfigurationType;

  @Column({
    nullable: false,
    enum: IoTTaskConfigurationDetailsType,
  })
  detailsType?: IoTTaskConfigurationDetailsType;

  @Column({
    nullable: false,
  })
  configuration?: string;

  @Column({
    nullable: false,
  })
  configurationDetails?: string;

  configurationData?: unknown;

  configurationDetailsData?: unknown;

  validate(): EntityValidationResult {
    const validation = super.validate();

    if (!this.measurement) {
      validation.validationErrors.push(
        `Property 'measurement' can not be empty`,
      );
    }

    if (!this.configurationType) {
      validation.validationErrors.push(
        `Property 'configurationType' can not be empty`,
      );
    }

    if (!this.detailsType) {
      validation.validationErrors.push(
        `Property 'detailsType' can not be empty`,
      );
    }

    if (!this.configuration) {
      validation.validationErrors.push(
        `Property 'configuration' can not be empty`,
      );
    }

    if (!this.configurationDetails) {
      validation.validationErrors.push(
        `Property 'configurationDetails' can not be empty`,
      );
    }

    return {
      isValid: validation.validationErrors.length === 0,
      validationErrors: validation.validationErrors,
    };
  }
}
