import { Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { SensorEntity } from '../../entities/sensor.entity';
import { OnEvent } from '@nestjs/event-emitter';
import { ApplicationEventSubject } from '../../../common';
import { SensorCreatedEvent } from '../../../common/events/sensor-created.event';
import { SensorUpdatedEvent } from '../../../common/events/sensor-updated.event';
import { ApplicationEventsService } from '../../../common';

export class SensorService {
  private readonly _logger = new Logger(`IoTIngress/${SensorService.name}`);

  constructor(
    @InjectRepository(SensorEntity)
    private readonly _sensorEntityRepository: Repository<SensorEntity>,
  ) {}

  async findSensor(options: FindOneOptions<SensorEntity>) {
    return this._sensorEntityRepository.findOne(options);
  }

  @OnEvent(ApplicationEventSubject.SensorCreated)
  async onSensorCreated(event: SensorCreatedEvent['payload']) {
    this._logger.verbose(
      ApplicationEventsService.formatReceiverLog(
        ApplicationEventSubject.SensorCreated,
        event,
      ),
    );
    try {
      await this._sensorEntityRepository.save({
        identifier: event.identifier,
        testingMode: event.testingMode,
        valueType: event.valueType,
        parentDevice: event.parentDevice,
      });
    } catch (err) {
      this._logger.error(err);
    }
  }

  @OnEvent(ApplicationEventSubject.SensorUpdated)
  async onSensorUpdated(event: SensorUpdatedEvent['payload']) {
    this._logger.verbose(
      ApplicationEventsService.formatReceiverLog(
        ApplicationEventSubject.SensorUpdated,
        event,
      ),
    );
    try {
      const updatableFields = [
        'parentDevice',
        'activeMeasurement',
        'testingMode',
      ];

      const fieldsToBeUpdated = {};
      for (const field of updatableFields) {
        if (event[field] !== undefined) {
          fieldsToBeUpdated[field] = event[field];
        }
      }

      await this._sensorEntityRepository.update(
        {
          identifier: event.identifier,
        },
        fieldsToBeUpdated,
      );
    } catch (err) {
      this._logger.error(err);
    }
  }
}
