import { Thing } from '../../common';
import { Column, Entity, ManyToOne } from 'typeorm';
import { SensorEntity } from './sensor.entity';
import { IoTActuatorEntity } from './iot-actuator.entity';

@Entity({
  name: 'IoTIngress_SensorMeasurementEntity',
})
export class SensorMeasurementEntity extends Thing {
  private static _AdditionalType = {
    SensorMeasurement: 'SensorMeasurement',
    ActuatorMeasurement: 'ActuatorMeasurement',
  };

  static get AdditionalType() {
    return this._AdditionalType;
  }

  @Column({
    unique: true,
  })
  identifier: string;

  @Column({
    nullable: true,
    enum: [
      SensorMeasurementEntity.AdditionalType.SensorMeasurement,
      SensorMeasurementEntity.AdditionalType.SensorMeasurement,
    ],
  })
  additionalType?: string;

  @Column({
    nullable: true,
  })
  isActive?: boolean;

  @Column({
    nullable: true,
  })
  isProduction?: boolean;

  @ManyToOne(() => SensorEntity, (sensor) => sensor.measurements, {
    nullable: true,
  })
  sensor?: SensorEntity;

  @ManyToOne(() => IoTActuatorEntity, (actuator) => actuator.measurements, {
    nullable: true,
  })
  actuator?: IoTActuatorEntity;
}
