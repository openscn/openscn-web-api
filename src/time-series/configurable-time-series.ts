import { ConfigurableModuleBuilder } from '@nestjs/common';
import { InfluxDBOptions } from './influxdb';

export const {
  ConfigurableModuleClass: ConfigurableTimeSeriesModule,
  MODULE_OPTIONS_TOKEN: IFLUXDB_OPTIONS_TOKEN,
} = new ConfigurableModuleBuilder<InfluxDBOptions>().build();
