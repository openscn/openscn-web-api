import { ConfigurableModuleBuilder, Module } from '@nestjs/common';
import { InfluxDBOptions } from './influxdb';
import { InfluxdbService } from './services/influxdb.service';
import { INFLUX_DB_OPTIONS } from './constants';
import { ConfigurableTimeSeriesModule } from './configurable-time-series';
import { HttpModule } from '@nestjs/axios';

export const {
  ConfigurableModuleClass,
  MODULE_OPTIONS_TOKEN: IFLUXDB_OPTIONS_TOKEN,
} = new ConfigurableModuleBuilder<InfluxDBOptions>().build();

@Module({
  imports: [HttpModule],
  providers: [
    InfluxdbService,
    {
      provide: 'INFLUX_DB_OPTIONS',
      useValue: {
        org: 'options.org',
        bucket: 'options.bucket',
      },
    },
  ],
  exports: [InfluxdbService, INFLUX_DB_OPTIONS],
})
export class TimeSeriesModule extends ConfigurableTimeSeriesModule {}
