import { ExecutionContext, Injectable, Logger } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable()
export class ValidJwtAuthGuard extends AuthGuard('jwt') {
  private readonly _logger = new Logger(`Auth/${ValidJwtAuthGuard.name}`);

  constructor(private readonly _authService: AuthService) {
    super();
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    return new Promise(async (resolve) => {
      try {
        const req = context.switchToHttp().getRequest();
        const token = req.headers['authorization'];
        req.tokenIntrospection = await this._authService.introspectToken(
          token?.split(' ')[1],
        );
        return resolve(true);
      } catch (err) {
        this._logger.error(err);
        return resolve(false);
      }
    });
  }
}
