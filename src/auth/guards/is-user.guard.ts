import { ExecutionContext, Injectable, Logger } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';
import { UsersService } from '../../users/services/users.service';
import { UserAccountService } from '../services/user-account.service';

@Injectable()
export class IsUserAuthGuard extends AuthGuard('local') {
  private readonly _logger = new Logger(`Auth/${IsUserAuthGuard.name}`);

  constructor(
    private readonly _userAccountService: UserAccountService,
    private readonly _userService: UsersService,
  ) {
    super();
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    return new Promise(async (resolve) => {
      const req = context.switchToHttp().getRequest();
      try {
        const userAccount = await this._userAccountService.findOne({
          where: {
            identifier: req.tokenIntrospection.identifier,
            email: req.tokenIntrospection.email,
            deletedDate: null,
            isActive: true,
          },
        });

        const user = await this._userService.findOne({
          where: {
            id: userAccount.userId,
          },
        });

        if (!user) {
          return resolve(false);
        } else {
          req.user = user;
          return resolve(true);
        }
      } catch (err) {
        this._logger.error(err);
        return resolve(false);
      }
    });

    return true;
  }
}
