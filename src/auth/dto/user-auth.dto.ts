import { IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { UserAccount } from '../entities/user-account.entity';

/**
 *
 * @class LoginInputDTO
 * @description Describes the input fields needed for a user to perform a login action
 *
 * @property {string} email The user's account email
 * @property {string} password The user's account password
 *
 */
export class LoginInputDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  @MinLength(3)
  @MaxLength(40)
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(40)
  password: string;
}

/**
 *
 * @class RegisterInputDTO
 * @description Describes the input fields needed for a new user to be registered
 *
 * @property {string} email The user's desired email
 * @property {string} password The user's desired password
 * @property {string} passwordRepeat A simple verification field
 *
 */
export class RegisterInputDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  @MinLength(3)
  @MaxLength(40)
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(40)
  password: string;

  isActive?: boolean;
}

export class RegisterWithTokenInputDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  @MinLength(3)
  @MaxLength(40)
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(40)
  password: string;

  @ApiProperty()
  @MaxLength(64)
  @MinLength(64)
  token: string;
}

export class ForgotPasswordDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  @MinLength(3)
  @MaxLength(40)
  email: string;
}

export class ResetPasswordDTO {
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(64)
  @MaxLength(64)
  token: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(40)
  password: string;
}

export class LoginResponseDTO {
  userAccount: UserAccount;
  token: string;
}

export class VerifyAccountDTO {
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(64)
  @MaxLength(64)
  token: string;
}

export class InviteUserDTO {
  @ApiProperty()
  @IsEmail()
  email: string;
}
