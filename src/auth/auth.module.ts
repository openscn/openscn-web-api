import { Module } from '@nestjs/common';
import { CommonModule } from '../common';
import { UsersModule } from '../users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserAccount } from './entities/user-account.entity';
import { UserAccountLocal } from './entities/user-account-local.entity';
import { AuthController } from './auth.controller';
import { AuthService } from './services/auth.service';
import { PasswordService } from './services/password.service';
import { UserAccountLocalService } from './services/user-account-local.service';
import { UserAccountService } from './services/user-account.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { IAuthConfig } from '../config/config.inteface';
import { ValidJwtAuthGuard } from './guards';
import { IsUserAuthGuard } from './guards';
import { PassportModule } from '@nestjs/passport';
import { AuthTokenService } from './services/auth-token.service';
import { AuthTokenEntity } from './entities/auth-token.entity';
import { EmailModule } from '../email/email.module';
import { UserInvitationController } from './controllers/user-invitation.controller';

@Module({
  imports: [
    CommonModule,
    UsersModule,
    EmailModule,
    TypeOrmModule.forFeature([UserAccount, UserAccountLocal, AuthTokenEntity]),
    PassportModule.register({
      defaultStrategy: 'jwt',
      session: false,
    }),
    JwtModule.registerAsync({
      useFactory: (configService: ConfigService) => {
        return {
          secret: (<IAuthConfig>configService.get('auth')).jwtSecret,
          signOptions: {
            expiresIn: (<IAuthConfig>configService.get('auth')).jwtExpiration,
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
  controllers: [AuthController, UserInvitationController],
  exports: [
    TypeOrmModule,
    AuthService,
    PasswordService,
    UserAccountService,
    ValidJwtAuthGuard,
    IsUserAuthGuard,
  ],
  providers: [
    AuthService,
    PasswordService,
    UserAccountLocalService,
    UserAccountService,
    ValidJwtAuthGuard,
    IsUserAuthGuard,
    AuthTokenService,
  ],
})
export class AuthModule {}
