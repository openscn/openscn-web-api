import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { IoTTaskThresholdCheckCondition } from '../../iot-tasks';
import { ResourceSearchQueryDTO } from '../../common';

export class IoTAlertConfigurationCreateDTO {
  @ApiProperty()
  @IsString()
  name?: string;

  @ApiProperty()
  @IsOptional()
  description?: string;

  @ApiProperty()
  @IsUUID()
  sensor: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  warningThreshold?: number;

  @ApiProperty()
  @IsNotEmpty()
  criticalThreshold: number;

  @ApiProperty()
  @IsEnum(IoTTaskThresholdCheckCondition)
  condition: IoTTaskThresholdCheckCondition;
}

export class IoTAlertConfigurationUpdateDTO {
  @ApiProperty()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty()
  @IsOptional()
  description?: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  warningThreshold?: number;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  criticalThreshold?: number;

  @ApiProperty()
  @IsOptional()
  @IsEnum(IoTTaskThresholdCheckCondition)
  condition: IoTTaskThresholdCheckCondition;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  isActive?: boolean;
}

export class IoTAlertConfigurationQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty()
  @IsOptional()
  @IsUUID()
  identifier?: string;

  @ApiProperty()
  @IsUUID()
  @IsOptional()
  sensor?: string;

  @ApiProperty()
  @IsOptional()
  isActive?: boolean;
}
