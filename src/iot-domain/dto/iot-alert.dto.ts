import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsIn, IsOptional, IsUUID } from 'class-validator';
import { ResourceSearchQueryDTO } from '../../common';

export class IotAlertCreateDTO {
  @ApiProperty()
  @IsUUID()
  alertConfiguration?: string;

  @ApiProperty()
  @IsIn(['normal', 'warning', 'critical', 'error'])
  status?: 'normal' | 'warning' | 'critical' | 'error';
}

export class IoTAlertUpdateDTO {
  @ApiProperty()
  @IsBoolean()
  acknowledged: boolean;
}

export class IoTAlertQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsUUID()
  identifier?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsIn(['normal', 'warning', 'critical', 'error'])
  status?: 'normal' | 'warning' | 'critical' | 'error';

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsUUID()
  configuration?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  acknowledged?: boolean;
}
