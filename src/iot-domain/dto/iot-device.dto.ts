import {
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ResourceSearchQueryDTO } from '../../common';

export class createIoTDeviceInputDTO {
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(2)
  @MaxLength(50)
  name: string;

  @ApiProperty({
    nullable: true,
  })
  @IsOptional()
  @MaxLength(500)
  description?: string;
}

export class UpdateIoTDeviceInputDTO {
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(2)
  @MaxLength(50)
  @IsOptional()
  name: string;

  @ApiProperty()
  @MaxLength(500)
  @IsOptional()
  description: string;
}

export class IoTDeviceDTO {
  name: string;
  description: string;
}

/**
 *
 * @class InvalidateToken
 * @description The invalidate token body
 *
 * @property {number} tokenId The id of the token
 *
 */
export class InvalidateTokenDTO {
  @ApiProperty()
  @IsUUID()
  identifier: string;
}

export class GenerateTokenDTO {
  @ApiProperty()
  @IsString()
  description: string;
}

export class IoTDeviceQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty()
  @IsOptional()
  @IsUUID()
  identifier?: string;
}
