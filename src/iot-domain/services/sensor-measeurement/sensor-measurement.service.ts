import { Injectable, Logger } from '@nestjs/common';
import {
  ApplicationEventsService,
  ApplicationEventSubject,
  EntityService,
  SensorMeasurementCreatedEvent,
  SensorMeasurementUpdatedEvent,
} from '../../../common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, FindOptionsWhere, Not, Repository } from 'typeorm';
import { SensorMeasurementEntity } from '../../entities/sensor-measurement.entity';
import { IoTAlertConfigurationService } from '../iot-alert-configuration/iot-alert-configuration.service';
import { IoTTaskService } from '../../../iot-tasks';

@Injectable()
export class SensorMeasurementService extends EntityService<SensorMeasurementEntity> {
  private readonly _logger = new Logger(
    `IoTDomain/${SensorMeasurementService.name}`,
  );

  constructor(
    @InjectRepository(SensorMeasurementEntity)
    private readonly sensorMeasurementRepository: Repository<SensorMeasurementEntity>,
    private readonly _applicationEventsService: ApplicationEventsService,
    private readonly _iotAlertConfigurationService: IoTAlertConfigurationService,
    private readonly _iotTaskService: IoTTaskService,
  ) {
    super(SensorMeasurementEntity.name);
    this.repository = sensorMeasurementRepository;
  }

  async create(
    item: DeepPartial<SensorMeasurementEntity>,
  ): Promise<SensorMeasurementEntity> {
    if (item.sensor) {
      item.additionalType =
        SensorMeasurementEntity.AdditionalType.SensorMeasurement;
    } else if (item.actuator) {
      item.additionalType =
        SensorMeasurementEntity.AdditionalType.ActuatorMeasurement;
    } else {
      throw new Error(`Could not resolve type for ${this.name}`);
    }

    return new SensorMeasurementEntity(item);
  }

  async save(item: SensorMeasurementEntity) {
    if (
      isNaN(item.id) &&
      item.identifier &&
      !isNaN(item.id) &&
      !item.identifier
    ) {
      throw new Error(
        `${this.name} must have an id and an identifier or neither`,
      );
    }
    const validation = item.validate();

    if (!validation.isValid) {
      throw new Error(validation.validationErrors.join(', '));
    }

    const action = isNaN(item.id) ? 'create' : 'update';
    const isActive = item.isActive;
    item.isActive = false;

    const result = await super.save(item);

    let event: SensorMeasurementCreatedEvent | SensorMeasurementUpdatedEvent;
    if (action === 'create') {
      event = {
        subject: ApplicationEventSubject.SensorMeasurementCreated,
        payload: {
          measurement: result.identifier,
          sensor: result?.sensor?.identifier,
          actuator: result?.actuator?.identifier,
          owner: result.owner.identifier,
          isActive: result.isActive,
          isProduction: result.isProduction,
          additionalType: result.additionalType,
          version: result.version,
        },
      };
    } else {
      event = {
        subject: ApplicationEventSubject.SensorMeasurementUpdated,
        payload: {
          measurement: result.identifier,
          isActive: result.isActive,
          isProduction: result.isProduction,
          version: result.version,
        },
      };
    }

    this._applicationEventsService.emit(event);

    if (isActive) {
      await this.setAsActive({
        id: result.id,
      });
    }

    return this.create(result);
  }

  async createAndSave(
    item: DeepPartial<SensorMeasurementEntity> | SensorMeasurementEntity,
  ) {
    return this.save(await this.create(item));
  }

  async update(id, fields) {
    const result = await super.update(id, fields);
    const target = await this.findOne({
      where: {
        id: id,
      },
    });

    if (result.affected > 0) {
      const payload: Record<string, unknown> = {};

      if (typeof fields?.isActive !== 'undefined') {
        payload.isActive = fields.isActive;
      }

      const event: SensorMeasurementUpdatedEvent = {
        subject: ApplicationEventSubject.SensorMeasurementUpdated,
        payload: {
          measurement: target.identifier,
          ...payload,
          version: target.version,
        },
      };

      this._applicationEventsService.emit(event);
    }

    return result;
  }

  async setAsActive(options: FindOptionsWhere<SensorMeasurementEntity>) {
    if (isNaN(Number(options.id))) {
      throw new Error(
        'SensorMeasurementService::setAsActive options parameter does not have the id property',
      );
    }

    let targetMeasurement: SensorMeasurementEntity;
    let pastMeasurements: SensorMeasurementEntity[];
    await this.sensorMeasurementRepository.manager.transaction(
      async (entityManager) => {
        targetMeasurement = await entityManager
          .getRepository(SensorMeasurementEntity)
          .findOne({
            where: options,
            relations: ['owner', 'sensor', 'actuator'],
          });

        if (!targetMeasurement) {
          throw new Error(
            `SensorMeasurementService::setAsActive can not find SensorMeasurement with id ${options.id}`,
          );
        } else if (!this.additionalTypeIsValid(targetMeasurement)) {
          throw new Error(
            `SensorMeasurementService::setAsActive called for a SensorMeasurement with invalid additionalType`,
          );
        }

        targetMeasurement = await entityManager
          .getRepository(SensorMeasurementEntity)
          .save({
            ...targetMeasurement,
            isActive: true,
          });

        const remainingOptions: FindOptionsWhere<SensorMeasurementEntity> = {
          owner: {
            id: targetMeasurement.owner.id,
          },
          id: Not(targetMeasurement.id),
          additionalType: targetMeasurement.additionalType,
          isActive: true,
        };

        if (
          targetMeasurement.additionalType ===
          SensorMeasurementEntity.AdditionalType.SensorMeasurement
        ) {
          remainingOptions.sensor = {
            id: targetMeasurement.sensor.id,
          };
        } else if (
          targetMeasurement.additionalType ===
          SensorMeasurementEntity.AdditionalType.ActuatorMeasurement
        ) {
          remainingOptions.actuator = {
            id: targetMeasurement.actuator.id,
          };
        } else {
          throw new Error('kaboom!');
        }

        pastMeasurements = await this.findAll({
          where: remainingOptions,
          relations: ['owner', 'sensor', 'actuator'],
        });

        const updateRequests = pastMeasurements.map((measurement) =>
          entityManager.getRepository(SensorMeasurementEntity).save({
            ...measurement,
            isActive: false,
          }),
        );

        pastMeasurements = await Promise.all(updateRequests);
      },
    );

    const targetSensorUpdatedEvent: SensorMeasurementUpdatedEvent = {
      subject: ApplicationEventSubject.SensorMeasurementUpdated,
      payload: {
        measurement: targetMeasurement.identifier,
        isActive: targetMeasurement.isActive,
        isProduction: targetMeasurement.isProduction,
        version: targetMeasurement.version,
      },
    };

    this._applicationEventsService.emit(targetSensorUpdatedEvent);

    for (let i = 0; i < pastMeasurements.length; i++) {
      const event: SensorMeasurementUpdatedEvent = {
        subject: ApplicationEventSubject.SensorMeasurementUpdated,
        payload: {
          measurement: pastMeasurements[i].identifier,
          isActive: pastMeasurements[i].isActive,
          isProduction: pastMeasurements[i].isProduction,
          version: pastMeasurements[i].version,
        },
      };

      this._applicationEventsService.emit(event);
    }

    if (targetMeasurement?.sensor) {
      const alertConfigurations =
        await this._iotAlertConfigurationService.findAll({
          where: {
            sensor: {
              identifier: targetMeasurement.sensor.identifier,
            },
          },
          relations: ['task'],
        });

      const reqs = [];

      for (let i = 0; i < alertConfigurations.length; i++) {
        reqs.push(
          this._iotTaskService.save({
            ...alertConfigurations[i].task,
            measurement: targetMeasurement.identifier,
          }),
        );
      }

      await Promise.all(reqs);
    }
  }

  additionalTypeIsValid(measurement: SensorMeasurementEntity): boolean {
    return (
      measurement?.additionalType ===
        SensorMeasurementEntity.AdditionalType.SensorMeasurement ||
      measurement?.additionalType ===
        SensorMeasurementEntity.AdditionalType.ActuatorMeasurement
    );
  }

  disableIoTAlertConfigurations(measurement: SensorMeasurementEntity) {
    return this._iotAlertConfigurationService
      .findAll({
        where: {
          sensor: {
            id: measurement.id,
          },
        },
      })
      .then(async (configs) => {
        if (configs.length > 0) {
          const reqs = [];

          for (let i = 0; i < configs.length; i++) {
            reqs.push(
              this._iotAlertConfigurationService
                .save({
                  ...configs[i],
                  isActive: false,
                })
                .then(() =>
                  this._logger.log(
                    `Set IoTAlertConfiguration property 'isActive' as false  as result of updating SensorMeasurement ${measurement.identifier}`,
                  ),
                ),
            );
          }

          return await Promise.all(reqs).catch((err) =>
            this._logger.error(err.stack ?? err),
          );
        }
      })
      .catch((err) => this._logger.error(err.stack ?? err));
  }
}
