import { Injectable, Logger } from '@nestjs/common';
import {
  ApplicationEventsService,
  ApplicationEventSubject,
  EntityService,
  IIoTAlertCreatedEvent,
} from '../../../common';
import { IoTAlertEntity } from '../../entities/iot-alert.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import { IIoTAlertUpdatedEvent } from '../../../common/events/iot-alert/iot-alert-updated.event';

@Injectable()
export class IoTAlertService extends EntityService<IoTAlertEntity> {
  private readonly _logger = new Logger(`IoTDomain/${IoTAlertService.name}`);

  constructor(
    @InjectRepository(IoTAlertEntity)
    private readonly _repository: Repository<IoTAlertEntity>,
    private readonly _applicationEventsService: ApplicationEventsService,
  ) {
    super(IoTAlertEntity.name);
    this.repository = _repository;
  }

  async save(item: DeepPartial<IoTAlertEntity>): Promise<IoTAlertEntity> {
    if (item.id && item.acknowledged) {
      return super
        .save(item)
        .then((result) => {
          const event: IIoTAlertUpdatedEvent = {
            subject: ApplicationEventSubject.IoTAlertUpdated,
            payload: {
              alert: item.identifier,
              status: item.status,
              acknowledged: item.acknowledged,
              alertConfiguration: item?.alertConfiguration?.identifier,
              prevStatus: item.prevStatus,
            },
          };

          this._applicationEventsService.emit(event);

          return result;
        })
        .catch((err) => {
          this._logger.error(err?.stack ?? err);
          return null;
        });
    } else {
      return super.save(item).then((result) => {
        const event: IIoTAlertCreatedEvent = {
          subject: ApplicationEventSubject.IoTAlertCreated,
          payload: {
            alert: result.identifier,
            status: result.status,
            prevStatus: result.prevStatus,
            alertConfiguration: result.alertConfiguration.identifier,
            value: result.value,
          },
        };

        this._applicationEventsService.emit(event);
        return result;
      });
    }
  }

  async create(item: DeepPartial<IoTAlertEntity>) {
    const instance = new IoTAlertEntity(item);
    const validation = instance.validate();

    if (!validation.isValid) {
      throw new Error(validation.validationErrors.join(','));
    }

    return instance;
  }
}
