import {
  ApplicationEventSubject,
  EntityService,
  IIoTDeviceTokenCreatedEvent,
  IIoTDeviceTokenRemovedEvent,
  IIoTDeviceTokenUpdatedEvent,
  IoTDeviceAccessTokenType,
} from '../../../common';
import {
  IotDeviceAccessTokenEntity,
} from '../../entities/iot-device-access-token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOptionsWhere, Repository, UpdateResult } from 'typeorm';
import * as crypto from 'crypto';
import { IotDeviceEntity } from '../../entities/iot-device.entity';
import { ApplicationEventsService } from '../../../common/services/application-events.service';
import { Logger } from '@nestjs/common';

export class IotDeviceAccessTokenService extends EntityService<IotDeviceAccessTokenEntity> {
  private readonly _logger = new Logger(
    `IoTDomain/${IotDeviceAccessTokenEntity.name}`,
  );

  constructor(
    @InjectRepository(IotDeviceAccessTokenEntity)
    private iotDeviceAccessTokenEntityRepository: Repository<IotDeviceAccessTokenEntity>,
    private readonly _applicationEventsService: ApplicationEventsService,
  ) {
    super(IotDeviceAccessTokenEntity.name);
    this.repository = iotDeviceAccessTokenEntityRepository;
  }

  /**
   *
   * Generates a new access token string
   *
   */
  generate(): string {
    const buffer = crypto.randomBytes(48);
    return buffer.toString('hex');
  }

  /**
   *
   * Assigns an AccessToken to a device
   *
   * @param device The target device
   * @param token The token to be used
   * @param tokenDescription
   * @param options Optional params
   *
   */
  async attachToDevice(
    device: IotDeviceEntity,
    token: string,
    tokenDescription?: string,
    options?: { isValid?: boolean; ttl?: number },
  ): Promise<IotDeviceAccessTokenEntity> {
    const tokenEntity = await this.save({
      owner: device.owner,
      token,
      device,
      description: tokenDescription,
      additionalType: IoTDeviceAccessTokenType.HTTPAccess,
      ...options,
    });

    const event: IIoTDeviceTokenCreatedEvent = {
      subject: ApplicationEventSubject.IoTDeviceTokenCreated,
      payload: {
        identifier: tokenEntity.identifier,
        isValid: tokenEntity.isValid,
        token: tokenEntity.token,
        device: device.identifier,
        additionalType: tokenEntity.additionalType,
      },
    };

    this._applicationEventsService.emit(event);

    return tokenEntity;
  }

  /**
   *
   * Invalidates a IoTDeviceAccessToken
   *
   * @param id The id of the target IoTDeviceAccessToken
   */
  async invalidateToken(id: number): Promise<UpdateResult> {
    const result = await this.update(id, {
      isValid: false,
    });

    const token = await this.findOne({
      where: {
        id: id,
      },
    });

    const event: IIoTDeviceTokenUpdatedEvent = {
      subject: ApplicationEventSubject.IoTDeviceTokenUpdated,
      payload: {
        identifier: token.identifier,
        isValid: false,
      },
    };

    this._applicationEventsService.emit(event);

    return result;
  }

  async softRemove(criteria: FindOptionsWhere<IotDeviceAccessTokenEntity>) {
    const tokens = await this.iotDeviceAccessTokenEntityRepository.find({
      where: criteria,
      withDeleted: true,
    });

    const result = await super.softRemove(criteria);
    if (result.affected > 0) {
      for (let i = 0; i < tokens?.length; i++) {
        const event: IIoTDeviceTokenRemovedEvent = {
          subject: ApplicationEventSubject.IoTDeviceTokenRemoved,
          payload: {
            identifier: tokens[i].identifier,
          },
        };

        this._applicationEventsService.emit(event);
      }
    } else {
      this._logger.error(
        `SoftDelete of ${JSON.stringify(
          criteria,
        )} did not have any affected rows`,
      );
    }

    return result;
  }
}
