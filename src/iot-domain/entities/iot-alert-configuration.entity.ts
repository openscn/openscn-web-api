import { OwnedThing } from '../../users/entities/owned-thing.entity';
import { SensorEntity } from './sensor.entity';
import {
  Column,
  DeepPartial,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from 'typeorm';
import { IoTTaskEntity } from '../../iot-tasks';
import { EntityValidationResult } from '../../common';

@Entity({
  name: 'IoTDomain_IoTAlertConfigurationEntity',
})
export class IoTAlertConfigurationEntity extends OwnedThing {
  constructor(params?: DeepPartial<IoTAlertConfigurationEntity>) {
    super(params);

    if (params) {
      this.name = params.name;
      this.isActive = params.isActive;
      this.sensor = new SensorEntity(params.sensor);
      this.task = new IoTTaskEntity(params.task);
    }
  }

  @Column({
    nullable: false,
  })
  name?: string;

  @Column({
    default: true,
    nullable: false,
  })
  isActive?: boolean;

  @ManyToOne(() => SensorEntity, {
    onDelete: 'CASCADE',
  })
  sensor?: SensorEntity;

  @OneToOne(() => IoTTaskEntity, {
    onDelete: 'CASCADE',
    persistence: false,
  })
  @JoinColumn()
  task: IoTTaskEntity;

  validate(): EntityValidationResult {
    const validation = super.validate();

    return {
      validationErrors: validation.validationErrors,
      isValid: validation.validationErrors.length === 0,
    };
  }
}
