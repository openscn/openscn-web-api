import { OwnedThing } from '../../users/entities/owned-thing.entity';
import { Column, DeepPartial, Entity, ManyToOne } from 'typeorm';
import { EntityValidationResult, IoTValue } from '../../common';
import { IotActuatorTriggerConfigurationEntity } from './iot-actuator-trigger-configuration.entity';
import { IoTAlertConfigurationEntity } from './iot-alert-configuration.entity';
import { IoTAlertEntity } from './iot-alert.entity';

@Entity({
  name: 'IoTDomain_IotActuatorTriggerActionEntity',
})
export class IotActuatorTriggerActionEntity extends OwnedThing {
  constructor(params?: DeepPartial<IotActuatorTriggerActionEntity>) {
    super(params);

    if (params) {
      this.configuration = new IotActuatorTriggerConfigurationEntity(
        params.configuration,
      );
      this.alert = new IoTAlertEntity(params.alert);
      this.previousState = params.previousState;
      this.targetValue = params.targetValue;
    }
  }

  @ManyToOne(() => IotActuatorTriggerConfigurationEntity, {
    onDelete: 'CASCADE',
  })
  configuration?: IotActuatorTriggerConfigurationEntity;

  @ManyToOne(() => IoTAlertEntity, {
    onDelete: 'CASCADE',
  })
  alert?: IoTAlertEntity;

  @Column({
    enum: ['normal', 'warning', 'critical', 'error'],
  })
  previousState?: 'normal' | 'warning' | 'critical' | 'error';

  @Column({
    default: false,
  })
  targetValue: IoTValue;

  validate(): EntityValidationResult {
    const validation = super.validate();

    return {
      validationErrors: validation.validationErrors,
      isValid: validation.validationErrors.length === 0,
    };
  }
}
