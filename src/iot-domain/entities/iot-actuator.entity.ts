import {
  Column,
  DeepPartial,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { OwnedThing } from '../../users/entities/owned-thing.entity';
import { IotDeviceEntity } from './iot-device.entity';
import { SensorLabelEntity } from './sensor-label.entity';
import { SensorMeasurementEntity } from './sensor-measurement.entity';
import { EntityValidationResult, IoTValue } from '../../common';

@Entity({
  name: 'IoTDomain_IoTActuator',
})
export class IoTActuatorEntity extends OwnedThing {
  constructor(props?: DeepPartial<IoTActuatorEntity>) {
    super(props);

    if (props) {
      this.name = props.name;
      this.valueType = props.valueType ?? 'float';
      this.lowerLimit = props.lowerLimit;
      this.upperLimit = props.upperLimit;
      this.reportFormat = props.reportFormat ?? 'json';
      this.parentDevice = props.parentDevice as IotDeviceEntity;
      this.labels = props.labels as SensorLabelEntity[];
      this.measurements = props.measurements as SensorMeasurementEntity[];
    }
  }

  @Column()
  name: string;

  @Column({
    nullable: false,
    default: 'float',
    enum: ['int', 'float', 'boolean', 'string'],
  })
  valueType: IoTValue;

  @Column({
    nullable: true,
  })
  lowerLimit?: number;

  @Column({
    nullable: true,
  })
  upperLimit?: number;

  @Column({
    default: 'json',
    enum: ['raw', 'json'],
  })
  reportFormat: 'raw' | 'json';

  @ManyToOne(() => IotDeviceEntity, (device) => device.actuators)
  parentDevice: any;

  @ManyToMany(() => SensorLabelEntity)
  @JoinTable({
    name: 'IoTDomain_Actuator_SensorLabel',
  })
  labels: SensorLabelEntity[];

  @OneToMany(
    () => SensorMeasurementEntity,
    (measurement) => measurement.actuator,
  )
  measurements: SensorMeasurementEntity[];

  validate(): EntityValidationResult {
    const errors = [];
    const validLimits = this.validateLimits();
    const reportFormatOk =
      this.reportFormat === 'raw' || this.reportFormat === 'json';

    if (!validLimits) {
      errors.push(`Limits violation`);
    }

    if (!reportFormatOk) {
      errors.push('Report format must be either raw or json');
    }

    return {
      isValid: errors.length === 0,
      validationErrors: errors,
    };
  }

  validateLimits() {
    if (this.valueType === 'int' || this.valueType === 'float') {
      const lower = Number(this.lowerLimit);
      const upper = Number(this.upperLimit);

      return !isNaN(lower) && !isNaN(upper) ? lower <= upper : true;
    } else {
      return true;
    }
  }

  validateValue(value: IoTValue): boolean {
    if (this.valueType === 'int' || this.valueType === 'float') {
      return typeof value === 'number';
    } else if (this.valueType === 'boolean') {
      return typeof value === 'boolean';
    } else if (this.valueType === 'string') {
      return typeof value === 'string';
    }
  }
}
