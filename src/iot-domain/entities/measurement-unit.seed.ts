export const commonMeasurementUnitSeed = [
  {
    id: 1,
    name: 'Per cent',
    description: 'A percentage',
    symbol: '%',
  },
  {
    id: 2,
    name: 'Temperature',
    description: 'Temperature in Celsius',
    symbol: '°C',
  },
  {
    id: 3,
    name: 'Short distance',
    description: 'Distance in cm',
    symbol: 'cm',
  },
];
