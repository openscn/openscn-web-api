import { Column, Entity } from 'typeorm';
import { OwnedThing } from '../../users/entities/owned-thing.entity';

/**
 *
 * @class SensorLabelEntity
 * @extends OwnedThing
 * @description Declares a SensorLabel. SensorLabels are assigned to sensors in order to allow the end user to query
 * multiple sensors that matches certain criteria at once with ease
 *
 * @property {string} name The name of the label
 *
 */
@Entity({
  name: 'IoTDomain_SensorLabel',
})
export class SensorLabelEntity extends OwnedThing {
  @Column()
  name: string;
}
