import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class ResourceSearchQueryDTO {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  q?: string;
}
