/**
 *
 * @abstract
 * @class EntityBase
 *
 * Is being used to indicate that a class will be used as an entity using the typeORM library
 *
 */
export abstract class EntityBase {}
