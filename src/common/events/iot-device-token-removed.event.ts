import { ApplicationEventSubject, IApplicationEvent } from '../interfaces';

export interface IIoTDeviceTokenRemovedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.IoTDeviceTokenRemoved;
  payload: {
    identifier: string;
  };
}
