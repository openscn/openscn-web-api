import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
} from '../../interfaces';

/**
 *
 * @interface IActuatorRemovedEventPayload
 * @description The payload of an IActuatorRemovedEvent
 *
 * @property {string} actuator The public identifier of the IoTActuator entity instance
 *
 */
interface IActuatorRemovedEventPayload extends IApplicationEventPayload {
  actuator: string;
}

/**
 *
 * @interface IActuatorRemovedEvent
 * @description Describes the removal of an IoTActuator
 *
 * @property {string} subject The event subject
 * @property {IActuatorRemovedEventPayload} The payload
 *
 */
export interface IActuatorRemovedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.ActuatorRemoved;
  payload: IActuatorRemovedEventPayload;
}
