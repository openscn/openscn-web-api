import { MeasurementUnitEntity } from 'src/iot-domain/entities/measurement-unit.entity';
import { ApplicationEventSubject, IApplicationEvent } from '../interfaces';

export interface SensorCreatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorCreated;
  payload: {
    owner: string;
    identifier: string;
    parentDevice?: string;
    valueType?: 'int' | 'float' | 'string' | 'boolean';
    measurementUnit?: MeasurementUnitEntity;
    activeMeasurement?: string;
    testingMode?: boolean;
  };
}
