import { ApplicationEventSubject, IApplicationEvent } from '../interfaces';

export interface IIoTDeviceTokenUpdatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.IoTDeviceTokenUpdated;
  payload: {
    identifier: string;
    isValid: boolean;
  };
}
