import { ApplicationEventSubject, IApplicationEvent } from '../interfaces';

export interface SensorMeasurementRemovedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorMeasurementRemoved;
  payload: {
    measurement: string;
  };
}
