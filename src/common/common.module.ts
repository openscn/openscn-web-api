import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Person } from './entities';
import { PersonService } from './services/person.service';
import { EncapsulatedErrorService, QueryService } from './services';
import { ApplicationEventsService } from './services';
import { ApplicationEventEntity } from './entities/application-event.entity';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forFeature([Person, ApplicationEventEntity]),
  ],
  exports: [
    TypeOrmModule,
    PersonService,
    EncapsulatedErrorService,
    ApplicationEventsService,
    QueryService,
  ],
  providers: [
    PersonService,
    EncapsulatedErrorService,
    ApplicationEventsService,
    QueryService,
  ],
})
export class CommonModule {}
