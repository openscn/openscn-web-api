import { IsNotEmpty, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export interface RawIoTValue {
  result: string;
  table: number;
  _start: string;
  _stop: string;
  _time: string;
  _value: number | string | boolean;
  _field: 'value';
  _measurement: string;
  sensor_id: string;
}

export interface IoTValue {
  result: string;
  table: number;
  start: string;
  stop: string;
  time: string;
  value: number | string | boolean;
  field: 'value';
  measurement: string;
  sensor_id: string;
}

export class IoTQueryDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  measurement: string;

  @ApiProperty()
  @IsNotEmpty()
  start?: number;

  @ApiProperty()
  stop?: number;
}
