# Contributing

In order to contribute to this project

- Fork the project
- Push your work into a branch on your repo
- Submit a Merge Request for that branch


Notes:
- We use githookd to ensure code quolity. You can disable them by adding the `--no-verify` flag to either `git commit` or `git push`

# Installation

- clone the project
- Run `nvm use`
- Run `npm install` 
- Run `docker-compose up` 


Notes:
- The docker instance tracks the node_modules changes. So install anything at 
your console.


# Services

- web_server: It has the business logic for the web interface and acts as API Gateway
- database: Holds the data for the web interface
- nosqldb: Is used to hold the IoT data
- adminer: A DB explorer
- postwoman: An API testing tol


# How to use adminer

Navigate to `localhost:8000`

- System: 'Postgres'
- Server: 'database'
- Username: 'openscn'
- Password: 'opsnscn'
- Database: 'openscn'

> Adminer config can be found at the `docker-compose.yml` file also



# Utilities

- You can drop and restore the DB by visiting thr `localhost:3000/db-drop` url.
