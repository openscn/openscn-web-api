const { query } = require("express");
const { MigrationInterface, QueryRunner } = require("typeorm");

module.exports = class AddAccessTokenType1718110018929 {
    name = 'AddAccessTokenType1718110018929'

    async up(queryRunner) {
        await queryRunner.query(`CREATE TYPE "public"."IoTDomain_IoTDeviceAccessToken_additionaltype_enum" AS ENUM('http-access', 'mqtt-access')`);
        await queryRunner.query(`ALTER TABLE "IoTDomain_IoTDeviceAccessToken" ADD "additionalType" "public"."IoTDomain_IoTDeviceAccessToken_additionaltype_enum"`);
        await queryRunner.query(`UPDATE "IoTDomain_IoTDeviceAccessToken" SET "additionalType" = 'http-access'`);
        await queryRunner.query(`ALTER TABLE "IoTDomain_IoTDeviceAccessToken" ALTER COLUMN "additionalType" SET NOT NULL`);
        
        await queryRunner.query(`CREATE TYPE "public"."IoTIngress_DeviceToken_additionaltype_enum" AS ENUM('http-access', 'mqtt-access')`);
        await queryRunner.query(`ALTER TABLE "IoTIngress_DeviceToken" ADD "additionalType" "public"."IoTIngress_DeviceToken_additionaltype_enum"`);
        await queryRunner.query(`UPDATE "IoTIngress_DeviceToken" SET "additionalType" = 'http-access'`);
        await queryRunner.query(`ALTER TABLE "IoTIngress_DeviceToken" ALTER COLUMN "additionalType" SET NOT NULL`);
    }

    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE "IoTIngress_DeviceToken" DROP COLUMN "additionalType"`);
        await queryRunner.query(`DROP TYPE "public"."IoTIngress_DeviceToken_additionaltype_enum"`);
        await queryRunner.query(`ALTER TABLE "IoTDomain_IoTDeviceAccessToken" DROP COLUMN "additionalType"`);
        await queryRunner.query(`DROP TYPE "public"."IoTDomain_IoTDeviceAccessToken_additionaltype_enum"`);
    }
}
